package main

import (
	"database/sql"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"time"

	_ "github.com/lib/pq"
)

var (
	verboseLogging bool = false
)

func main() {
	inputStr := flag.String("input", "", "from which file to read input (default is STDIN)")
	concurrency := flag.Int("concurrency", 10, "how many queries to run concurrently")

	dbhost := flag.String("dbhost", "localhost", "where is the dbhost to connect")
	dbname := flag.String("dbname", "homework", "which dbname to connect")

	flag.BoolVar(&verboseLogging, "verbose", false, "showing internal logging (default not)")

	flag.Parse()

	db := connectDB(*dbhost, *dbname) // Fatal if unable to connect DB

	inputStream := os.Stdin
	var err error
	if *inputStr != "" {
		inputStream, err = os.Open(*inputStr)
		if err != nil {
			log.Fatal("unable to read input file: %q\n", *inputStr)
		}
	}

	records := make(chan *Record, 100)

	go parser(inputStream, records)

	d := NewDispatcher(db, *concurrency)
	d.Start(records)

	nr := 0
	var first_begin, last_end time.Time
	var durations []time.Duration
	var durationSum time.Duration
	for r := range d.stats {
		if r == nil {
			continue
		}

		nr++

		if first_begin.IsZero() || r.begin.Before(first_begin) {
			first_begin = r.begin
		}
		if last_end.IsZero() || r.end.After(last_end) {
			last_end = r.end
		}

		durations = append(durations, r.duration)
		durationSum += r.duration
	}
	totalProcessingTime := last_end.Sub(first_begin)

	sort.Slice(durations, func(i, j int) bool { return durations[i] < durations[j] })

	minQueryTime := durations[0]
	maxQueryTime := durations[nr-1]
	medianQueryTime := durations[nr/2]
	if nr%2 == 0 {
		medianQueryTime = (durations[nr/2-1] + durations[nr/2]) / 2
	}
	p90QueryTime := durations[nr*9/10]
	p99QueryTime := durations[nr*99/100]

	avgQueryTime := durationSum / time.Duration(nr)

	log.Printf("Total run %d qureies, use %q (from %s to %s), min=%q, max=%q, median=%q, (90percentile: %q, 99percentile: %q), avg=%q",
		nr, totalProcessingTime,
		first_begin.Format(time.RFC3339Nano), last_end.Format(time.RFC3339Nano),
		minQueryTime, maxQueryTime, medianQueryTime,
		p90QueryTime, p99QueryTime,
		avgQueryTime)
}

func connectDB(dbhost, dbname string) *sql.DB {
	connStr := fmt.Sprintf("host='%s' user=postgres password=password dbname='%s' sslmode=disable", dbhost, dbname)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	return db
}

type Record struct {
	hostname   string
	start, end time.Time
}

const timeLayout = "2006-01-02 15:04:05"

func parser(input io.Reader, out chan<- *Record) {
	var stats struct {
		nr     int
		parsed int
		errors int
	}

	inputCsv := csv.NewReader(input)
	for {
		rec, err := inputCsv.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("failed reading csv, %#v\n", err)
			break
		}
		stats.nr++

		if len(rec) != 3 {
			log.Printf("this reader is expecting 3 columns csv")
			continue
		}

		// skip the header
		if stats.nr == 1 &&
			rec[0] == "hostname" && rec[1] == "start_time" &&
			rec[2] == "end_time" {
			continue
		}

		hostname := rec[0]
		start_time, err := time.Parse(timeLayout, rec[1])
		if err != nil {
			stats.errors++
			continue
		}
		end_time, err := time.Parse(timeLayout, rec[2])
		if err != nil {
			stats.errors++
			continue
		}

		r := &Record{hostname, start_time, end_time}

		if verboseLogging {
			log.Printf("%v\n", r)
		}
		stats.parsed++

		out <- r
	}

	log.Printf("finished reading of %v lines\n", stats)

	close(out)
}
