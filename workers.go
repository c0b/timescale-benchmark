package main

import (
	"database/sql"
	"fmt"
	"log"
	"sort"
	"sync"
	"time"
)

type dispatcher struct {
	db      *sql.DB
	workers []*worker
	stats   chan *queryStat

	// a map to make sure same hostname always dispatch to same channel/worker
	sw map[string]*worker

	concurrency int
	wg          sync.WaitGroup
}

func NewDispatcher(db *sql.DB, concurrency int) *dispatcher {
	return &dispatcher{
		db:      db,
		workers: make([]*worker, 0, concurrency),
		stats:   make(chan *queryStat, 100),
		sw:      make(map[string]*worker),

		concurrency: concurrency,
	}
}

func (d *dispatcher) Start(records <-chan *Record) {
	for i := 0; i < d.concurrency; i++ {
		w := &worker{name: fmt.Sprintf("worker%d", i+1), d: d,
			input: make(chan *Record, 100)}
		d.workers = append(d.workers, w)
		d.wg.Add(1)
		w.Start()
	}

	go func() {
		for r := range records {
			// select next worker
			w := d.selectWorker(r.hostname)
			w.Add(r)
		}

		for _, w := range d.workers {
			w.Stop()
		}
	}()

	go func() {
		d.wg.Wait()
		close(d.stats)
	}()
}

// if this hostname is known dispatching on a worker, always re-use it
// otherwise select a worker with minimum load (least number of hostnames)
func (d *dispatcher) selectWorker(hostname string) *worker {
	if w, ok := d.sw[hostname]; ok {
		return w
	}

	// if it's a new hostname, need to balance out the minimum load worker
	sort.Slice(d.workers, func(i, j int) bool { return len(d.workers[i].hostnames) < len(d.workers[j].hostnames) })
	w := d.workers[0]
	w.hostnames = append(w.hostnames, hostname)
	d.sw[hostname] = w

	log.Printf("on a new host %q, select %q to run qureies", hostname, w.name)

	return w
}

type worker struct {
	d         *dispatcher
	name      string
	hostnames []string
	input     chan *Record
}

func (w *worker) Start() {
	go func() {
		for r := range w.input {
			w.d.stats <- queryFunc(w, r)
		}
		w.d.wg.Done()
	}()
}

func (w *worker) Add(r *Record) {
	w.input <- r
}

func (w *worker) Stop() {
	close(w.input)
}

type queryStat struct {
	begin, end time.Time
	duration   time.Duration
	succeeded  bool

	// query result set
}

func queryFunc(w *worker, r *Record) *queryStat {

	begin := time.Now()
	rows, err := w.d.db.Query(`
SELECT host, DATE_TRUNC('minute', ts) AS ts_minute,
        MIN(usage), MAX(usage), COUNT(*)
FROM cpu_usage WHERE host=$1 AND ts BETWEEN $2 AND $3 GROUP BY 1,2 ORDER BY 1,2`,
		r.hostname, r.start, r.end)
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		var (
			host     string
			ts_minte time.Time
			min, max float64
			count    int
		)
		if err := rows.Scan(&host, &ts_minte, &min, &max, &count); err != nil {
			log.Printf("read error: %#v", err)
			return nil
		}

		if verboseLogging {
			log.Printf("worker: %q, host %q ts_minte %+v, %.2f, %.2f, %d\n", w.name, host, ts_minte, min, max, count)
		}
	}

	rows.Close()

	end := time.Now()

	return &queryStat{begin: begin, end: end, duration: end.Sub(begin), succeeded: true}
}
