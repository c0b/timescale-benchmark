
## A Benchmarking tool

This is a tool designed to run benchmark on DB's performance concurrently, with features/strategies like processor affinity, can be extended to more generic cases;

Run on command line:

```console
$ go run ./main.go ./workers.go -help
Usage of /tmp/go-build260493412/b001/exe/main:
  -concurrency int
    	how many queries to run concurrently (default 10)
  -dbhost string
    	where is the dbhost to connect (default "localhost")
  -dbname string
    	which dbname to connect (default "homework")
  -input string
    	from which file to read input (default is STDIN)
  -verbose
    	showing internal logging (default not)
```

Or can use `go build` to be built into a single binary to deploy to other envrionment for performance benchmarking.

    $ go build -o ./benchmark ./main.go ./workers.go

### Run Example

```console
$ go run ./main.go ./workers.go -dbhost 172.17.0.5 -input ~/tmp/TimescaleDB_coding_assignment-RD_eng_setup/query_params.csv
2019/04/05 23:39:18 on a new host "host_000008", select "worker1" to run qureies
2019/04/05 23:39:18 on a new host "host_000001", select "worker7" to run qureies
2019/04/05 23:39:18 on a new host "host_000002", select "worker8" to run qureies
2019/04/05 23:39:18 on a new host "host_000003", select "worker9" to run qureies
2019/04/05 23:39:18 on a new host "host_000000", select "worker10" to run qureies
2019/04/05 23:39:18 on a new host "host_000005", select "worker2" to run qureies
2019/04/05 23:39:18 on a new host "host_000006", select "worker3" to run qureies
2019/04/05 23:39:18 on a new host "host_000007", select "worker4" to run qureies
2019/04/05 23:39:18 on a new host "host_000004", select "worker5" to run qureies
2019/04/05 23:39:18 on a new host "host_000009", select "worker6" to run qureies
2019/04/05 23:39:18 finished reading of {201 200 0} lines
2019/04/05 23:39:19 Total run 200 qureies, use "140.709608ms"
 (from 2019-04-05T23:39:18.922324641Z to 2019-04-05T23:39:19.063034301Z),
 min="1.95359ms", max="22.44681ms",
 median="4.26909ms", (90percentile: "9.29882ms", 99percentile: "19.832843ms"),
 avg="5.650848ms"

$ ➸ time \time ./benchmark -dbhost 172.17.0.5 -input ~/tmp/TimescaleDB_coding_assignment-RD_eng_setup/query_params.csv -concurrency 5
2019/04/06 10:20:59 on a new host "host_000008", select "worker1" to run qureies
2019/04/06 10:20:59 on a new host "host_000001", select "worker2" to run qureies
2019/04/06 10:20:59 on a new host "host_000002", select "worker3" to run qureies
2019/04/06 10:20:59 on a new host "host_000003", select "worker4" to run qureies
2019/04/06 10:20:59 on a new host "host_000000", select "worker5" to run qureies
2019/04/06 10:20:59 on a new host "host_000005", select "worker5" to run qureies
2019/04/06 10:20:59 on a new host "host_000006", select "worker4" to run qureies
2019/04/06 10:20:59 on a new host "host_000007", select "worker3" to run qureies
2019/04/06 10:20:59 on a new host "host_000004", select "worker2" to run qureies
2019/04/06 10:20:59 on a new host "host_000009", select "worker1" to run qureies
2019/04/06 10:20:59 finished reading of {201 200 0} lines
2019/04/06 10:20:59 Total run 200 qureies, use "152.666292ms"
 (from 2019-04-06T10:20:59.445946821-05:00 to 2019-04-06T10:20:59.598613321-05:00),
 min="1.933029ms", max="15.36621ms",
 median="2.479033ms",
 (90percentile: "3.642772ms", 99percentile: "15.105827ms"),
 avg="3.021699ms"

0.04user 0.02system 0:00.15elapsed 46%CPU (0avgtext+0avgdata 9624maxresident)k
0inputs+0outputs (0major+1363minor)pagefaults 0swaps

real	0m0.161s
user	0m0.053s
sys	0m0.025s

```